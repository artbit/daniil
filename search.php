<?php 

    if (input::is_post()) { 
        
        $data = [
            'type_realty' => (int) input::post('type_realty'),
            'price_from' => (int) input::post('price_from'),
            'price_to' => (int) input::post('price_to'),
            'count_room' => input::post('count_room'),
            'with_photo' => (int) input::post('with_photo'),
        ];
        
        $html = file_get_contents("http://www.50.bn.ru/sale/city/flats");
        $dom = new DOMDocument;
        $dom->loadHTML($html);
        $sections = $dom->getElementsByTagName('section');
        
        $resultPars = [];
        foreach ($sections as $section) {
            
            foreach ($section->getElementsByTagName('tr') as $tr) {
                $temp = [];
                $flag = true;
                foreach ($tr->getElementsByTagName('td') as $td) {
                    if ($flag) {
                        $tempA = [];
                        foreach ($td->getElementsByTagName('a') as $a) {
                            $tempA[] = trim($a->textContent);
                        }
                        $temp[] = $tempA;
                        $flag = false;
                    } else {
                        $temp[] = trim($td->textContent); 
                    }
                }
                $resultPars[] = $temp;
            }
        }

        $searching = [];
        foreach ($resultPars as $item) {
            if ($data['with_photo']) {
                if (count($item[0]) < 2) {
                    continue;
                }
            }
            if (!$data['count_room']) {
                if (array_search((int)$item[1], $data['count_room']) === false) {
                    continue;
                }
            }
            $price = preg_replace('/[^\d]/', '', $item[5]);
            if ($data['price_from']) {
                if ($price < $data['price_from']) {
                    continue;
                }
            }
            if ($data['price_to']) {
                if ($price > $data['price_to']) {
                    continue;
                }
            }
            $temp = [];
            $temp['name'] = $item[0];
            $temp['count_room'] = $item[1];
            $temp['area'] = $item[2];
            $temp['floor'] = $item[3];
            $temp['type_building'] = $item[4];
            $temp['price'] = $item[5];
            $temp['type_realty'] = $data['type_realty'];
            $searching[] = $temp;
        }
    }

    $html = "<table>
        <th>
            Тип недвижиммости
        </th>
        <th>
            Название
        </th>
        <th>
            Есть фото
        </th>
        <th>
            Кол-во комнат
        </th>
        <th>
            Общ. / Жил. / Кух.
        </th>
        <th>
            Этаж
        </th>
        <th>
            Еип здания
        </th>
        <th>
            Стоимость
        </th>";
    foreach ($searching as $realty) {
        $html .= "<tr>
            <td><center>" . $realty['type_realty'] . "</center></td>
            <td><center>" . $realty['name'][0] . "</center></td>
            <td><center>" . (isset($realty['name'][1]) ? "Есть" : "Нет") . "</center></td>
            <td><center>" . $realty['count_room'] . "</center></td>
            <td><center>" . $realty['area'] . "</center></td>
            <td><center>" . $realty['floor'] . "</center></td>
            <td><center>" . $realty['type_building'] . "</center></td>
            <td><center>" . $realty['price'] . "</center></td>
        </tr>";
    }
    $html .= "</table>";

    echo $html;
?>

<?php
    class Input {
        public static function post($key, $def = null)
        {
            return isset($_POST[$key]) ? $_POST[$key] : $def;
        }

        public static function is_post()
        {
            return count($_POST) 
                || strtolower($_SERVER['REQUEST_METHOD']) == 'post';
        }   
    }
?>